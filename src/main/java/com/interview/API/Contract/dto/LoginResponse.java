package com.interview.API.Contract.dto;

public class LoginResponse {
    private String id;
    private String token;

    public LoginResponse(String id, String token) {
        this.id = id;
        this.token = token;
    }
}