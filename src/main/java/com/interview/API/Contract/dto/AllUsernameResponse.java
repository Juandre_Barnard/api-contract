package com.interview.API.Contract.dto;

public class AllUsernameResponse {
    private Long id;
    private String phone;

    public AllUsernameResponse(Long id, String phone) {
        this.id = id;
        this.phone = phone;
    }
}