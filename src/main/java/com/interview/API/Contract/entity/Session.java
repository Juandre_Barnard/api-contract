package com.interview.API.Contract.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class Session {
    private @Id @GeneratedValue Long session_id;
    private LocalDateTime expiry_date;
    private Long user_id;
    private String token_id;

    public Session(Long session_id, LocalDateTime expiry_date, Long user_id, String token_id) {
        this.session_id = session_id;
        this.expiry_date = expiry_date;
        this.user_id = user_id;
        this.token_id = token_id;
    }

    public Long getSession_id() {
        return session_id;
    }

    public void setSession_id(Long session_id) {
        this.session_id = session_id;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public String getToken_id() {
        return token_id;
    }

    public void setToken_id(String token_id) {
        this.token_id = token_id;
    }

    public LocalDateTime getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(LocalDateTime expiry_date) {
        this.expiry_date = expiry_date;
    }
}