package com.interview.API.Contract.service;

import java.util.List;

import com.interview.API.Contract.dto.AllUsernameResponse;
import com.interview.API.Contract.dto.ModifyUserRequest;
import com.interview.API.Contract.entity.User;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

public interface UserService {

    public @ResponseBody List<AllUsernameResponse> findAll();

	public User save(@RequestBody ModifyUserRequest credentials);
    
}