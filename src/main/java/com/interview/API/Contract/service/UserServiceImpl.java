package com.interview.API.Contract.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.interview.API.Contract.dto.AllUsernameResponse;
import com.interview.API.Contract.dto.ModifyUserRequest;
import com.interview.API.Contract.entity.User;
import com.interview.API.Contract.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    public @ResponseBody List<AllUsernameResponse> findAll() {

        List<User> it = userRepository.findAll();
        List<AllUsernameResponse> allUsers = new ArrayList<AllUsernameResponse>();
        it.forEach(e -> {
            allUsers.add(new AllUsernameResponse(e.getUser_id(), e.getPhone()));
        });
        return allUsers;
    }

	public User save(@RequestBody ModifyUserRequest credentials) {
        List<User> it = userRepository.findAll();
        List<User> foundUsers = it.stream()
            .filter( u -> u.getUsername().equals(credentials.getUsername()))
            .collect(Collectors.toList());

        
        if (foundUsers.isEmpty()) return null;
        
        userRepository.save(foundUsers.get(1));

        return foundUsers.get(1);
	}
}