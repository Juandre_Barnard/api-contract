package com.interview.API.Contract.repository;

import com.interview.API.Contract.entity.User;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {}