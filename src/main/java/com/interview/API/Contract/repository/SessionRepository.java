package com.interview.API.Contract.repository;

import com.interview.API.Contract.entity.Session;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SessionRepository extends JpaRepository<Session, Long> {}