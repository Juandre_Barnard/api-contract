package com.interview.API.Contract.controller;

import java.util.List;

import com.interview.API.Contract.dto.AllUsernameResponse;
import com.interview.API.Contract.dto.ModifyUserRequest;
import com.interview.API.Contract.entity.User;
import com.interview.API.Contract.service.UserService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    private final UserService repository;

    public UserController(UserService repo) {
        this.repository = repo;
    }
  
    @GetMapping("/users")
    List<AllUsernameResponse> all() {
        return repository.findAll();
    }

    @PutMapping("/users")
    User replaceUser(@RequestBody ModifyUserRequest newUser) {
            return repository.save(newUser);
      }
}