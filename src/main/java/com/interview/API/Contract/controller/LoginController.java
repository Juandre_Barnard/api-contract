package com.interview.API.Contract.controller;

import com.interview.API.Contract.dto.LoginCredentials;
import com.interview.API.Contract.dto.LoginResponse;
import com.interview.API.Contract.dto.LogoutRequest;
import com.interview.API.Contract.repository.SessionRepository;
import com.interview.API.Contract.service.UserService;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {
    private final UserService userRepository;
    private final SessionRepository sessionRepository;

    public LoginController(UserService urepo, SessionRepository srepo) {
        this.userRepository = urepo;
        this.sessionRepository = srepo;
    }

    @PostMapping(value = "/login")
    public @ResponseBody LoginResponse login(@RequestBody LoginCredentials cred ) {
        return null;
    }

    @PostMapping(value = "/logout/{id}")
    public void logout(@RequestBody LogoutRequest logoutRequest, @PathVariable Long id) {
        
    }
}