package com.interview.API.Contract.bean;

import com.interview.API.Contract.entity.User;
import com.interview.API.Contract.repository.UserRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
class LoadDatabase {
  private final static Logger log = LoggerFactory.getLogger(LoadDatabase.class);

  @Bean
  static CommandLineRunner initDatabase(UserRepository repository) {
    return args -> {
      log.info("Preloading " + repository.save(new User("juandre", "9999999999", "1")));
      log.info("Preloading " + repository.save(new User("tempuser", "1","1")));
    };
  }
}